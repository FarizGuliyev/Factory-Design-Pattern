public class Main {
    public static void main(String[] args) {
        //        -------------Shape Factory Created--------------
        ShapeFactory shapeFactory = new ShapeFactory();

        //        -------------Circle object created--------------
        Shape shape1 = shapeFactory.getShape("CIRCLE");
        shape1.draw();

        //        -------------Square object created--------------
        Shape shape2=shapeFactory.getShape("SQUARE");
        shape2.draw();

        //        -------------Rectangle object created--------------
        Shape shape3=shapeFactory.getShape("RECTANGLE");
        shape3.draw();

        System.out.println(shapeFactory.getShape("Salam"));
        System.out.println(shapeFactory.getShape(null));

    }
}